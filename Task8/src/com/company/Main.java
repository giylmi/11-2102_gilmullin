package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) throws IOException {
        // write your code here
        ArrayList<Student> list = new ArrayList<Student>();
        BufferedReader br = new BufferedReader(new FileReader("input.txt"));
        PrintWriter pw = new PrintWriter("output.txt");
        String s = br.readLine();
        while (s != null){
            String[] strings = s.split(" ");
            String name = strings[0];
            int score = Integer.valueOf(strings[1]);
            list.add(new Student(score, name));
            s = br.readLine();
        }
        Collections.sort(list);
        int i = 0;
        for (Student student: list){
                pw.println(student.getName());
        }
        pw.close();
    }
}
class Student implements Comparable<Student>{
    private int score;
    private String name;

    Student(int score, String name) {
        this.score = score;
        this.name = name;
    }

    int getScore() {
        return score;
    }

    void setScore(int score) {
        this.score = score;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Student o) {
        return this.name.compareTo(o.getName());
    }
}