package com.company;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) throws SQLException {
	// write your code here
        Connection connection = ConnectionFactory.getInstance().getConnection();
        Statement statement = connection.createStatement();
        String query;

        System.out.println("Выбрать имена и специальности всех работников, у которых специальность совпадает с работником по имени 'Viktor'");
        query = "SELECT name, job_title FROM employee WHERE job_title = (SELECT job_title FROM employee WHERE name = 'Viktor')";
        ResultSet rs = statement.executeQuery(query);
        while (rs.next())
            System.out.println(rs.getString(2) + " " + rs.getString(1));

        System.out.println("Найти работников с минимальной зарплатой");
        query = "SELECT name FROM employee WHERE salary = (SELECT MIN(salary) from employee)";
        rs = statement.executeQuery(query);
        while (rs.next())
            System.out.println(rs.getString(1));

        System.out.println("Выбрать имена работников, чья специальность совпадает с любым сотрудником отдела продаж('JetBrains')");
	/*TODO можно сразу специальность вытаскивать в подзапросе*/
        query = "SELECT name FROM employee WHERE job_title in (SELECT job_title from employee WHERE dept_no = (SELECT id FROM dept WHERE name = 'JetBrains'))";
        rs = statement.executeQuery(query);
        while (rs.next())
            System.out.println(rs.getString(1));

        System.out.println("Вывести всех сотрудников, у которых в отделе есть аналитик(job_title='analyst')");
        query = "SELECT name FROM employee WHERE dept_no in (SELECT distinct dept_no from employee WHERE job_title = 'analyst')";
        rs = statement.executeQuery(query);
        while (rs.next())
            System.out.println(rs.getString(1));

        System.out.println("Найти работников с зарплатой выше средней");
        query = "SELECT name FROM employee WHERE salary > (SELECT AVG(salary) from employee)";
        rs = statement.executeQuery(query);
        while (rs.next())
            System.out.println(rs.getString(1));

        System.out.println("Вывести количество аналитиков по каждому отделу");
        query = "SELECT COUNT(*) FROM employee WHERE job_title = 'analyst' GROUP BY dept_no";
        rs = statement.executeQuery(query);
        while (rs.next())
            System.out.println(rs.getString(1));
    }
}
