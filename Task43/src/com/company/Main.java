package com.company;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;
/*TODO добавь еще один латч, чтобы отслеживать финиш. 
Засекай время в основном потоке, а не в потоках-машинах.
В 44 то же самое
*/
public class Main {

    CountDownLatch latch, finish;
    AtomicLong start;
    Car[] cars;

    public static void main(String[] args) {
        // write your code here
        new Main().run();

    }

    public void run() {
        latch = new CountDownLatch(1);
        finish = new CountDownLatch(1);
        start = new AtomicLong(System.nanoTime());
        cars = new Car[10];
        for (int i = 0; i < 10; i++) {
            cars[i] = new Car(i + 1);
            cars[i].start();
        }
        latch.countDown();
        while (Thread.activeCount() != 2){
        }
        System.out.println("The best time is " + (System.nanoTime() - start.get()));
    }

    class Car extends Thread {
        private int n;

        Car(int n) {
            this.n = n;
        }

        public void run() {
            try {
                latch.await();
                for (int i = 1; i <= 20; i++)
                    System.out.println("Car №" + this.n + " at Lap №" + i);
                long cur = System.nanoTime();
                long best = aLong.get();
                while (cur < best && !aLong.compareAndSet(best, cur)){
                    best = aLong.get();
                }
                System.out.println("Car №" + this.n + " finished!");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
