/**
 * Created by adel on 27.04.2014.
 */

var managerApp = angular.module('managerApp', ['ui.bootstrap']);

managerApp.controller('ManagerController', function($scope, $http, $modal){
    $http.get("http://localhost:8080/api/user/list").success(function(data){
        $scope.users = data;
    });

    $http.get("http://localhost:8080/api/company/list").success(function(data){
        $scope.companies = data;
    });

    $http.get("http://localhost:8080/api/cv/list").success(function(data){
        $scope.cvs = data;
    });

    $http.get("http://localhost:8080/api/vacancy/list").success(function(data){
        $scope.vacancies = data;
    });


    $scope.uid = $scope.cid = '';
    $scope.checkActive = function(should, id){
        return should == id ? 'active' : '';
    };
    $scope.openDialog = function(which, id){
        switch (which){
            case 'cv':
                var modalInstance = $modal.open({
                    templateUrl: '../partials/cv_modal.html',
                    controller: cvController,
                    backdrop: 'static',
                    resolve: {
                        items: function(){
                            return $scope.cvs[0];
                        }
                    }
                });

                break;
        }
    }
});

var cvController = function($scope, $modalInstance, items){
    $scope.cv = items;

    $scope.ok = function(){
        $modalInstance.close();
    }

    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    }
};