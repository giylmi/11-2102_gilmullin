package com.company;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;

public class Mergerer extends RecursiveAction {

    private static final int SEQUENTIAL_THRESHOLD = 5;

    private final int[] data;
    private final int start;
    private final int end;

    public Mergerer(int[] data, int start, int end) {
        this.data = data;
        this.start = start;
        this.end = end;
    }

    public Mergerer(int[] data) {
        this(data, 0, data.length);
    }

    @Override
    protected void compute() {
        final int length = end - start;
        if (length < SEQUENTIAL_THRESHOLD) {
            computeDirectly();
            return;
        }
        final int split = length / 2;
        final Mergerer left = new Mergerer(data, start, start + split);
        final Mergerer right = new Mergerer(data, start + split, end);
    /*
    * left.fork();
    * right.fork();
    * */
        ForkJoinTask.invokeAll(left, right);
        right.join();
        left.join();
        Arrays.sort(data, start, end);
    }

    private void computeDirectly() {
        System.out.println(Thread.currentThread() + " computing: " + start
                + " to " + end);
        Arrays.sort(data, start, end);
    }

    public static void main(String[] args) {
        // create a random data set
        final int[] data = new int[10];
        final Random random = new Random();
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(100);
            System.out.print(data[i] + " ");
        }
        System.out.println();
        // submit the task to the pool
        final ForkJoinPool pool = new ForkJoinPool(4);
        final Mergerer finder = new Mergerer(data);
        pool.invoke(finder);
        System.out.println();
        for (int i = 0; i < data.length; i++)
            System.out.print(data[i] + " ");
    }
}