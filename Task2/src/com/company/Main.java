package com.company;

import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Cards cards = new Cards();
        cards.shuffle();
        System.out.println(cards.toString());
    }


}

class Cards{
    private Card[] cards = new Card[36];

    public Cards(){
        int i = 0;
        for (Suit suit: Suit.values())
            for (Value value: Value.values())
                cards[i++] = new Card(suit, value);
    }

    public void shuffle(){
        Random rnd = new Random();
        for (int i = cards.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            Card a = cards[index].clone();
            cards[index] = cards[i].clone();
            cards[i] = a.clone();
        }
    }

    public String toString(){
        String res = "[\n";
        for (int i = 0; i < cards.length - 1; i++)
            res += "    " + cards[i].toString() + ",\n";
        res += "    " + cards[cards.length - 1].toString() + "\n";
        res += "]";
        return res;
    }
}

class Card{
    private Suit suit;
    private Value value;

    Card(Suit suit, Value value) {
        this.suit = suit;
        this.value = value;
    }

    public String toString(){
        return value + " of " + suit;
    }

    public Card clone(){
        return new Card(suit, value);
    }
}

enum Suit{
    Hearts,
    Diamonds,
    Clubs,
    Spades;
}

enum Value{
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace;
}