package com.company;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

public class Main {

    CountDownLatch latch;
    AtomicLong aLong, start;
    Car[] cars;

    public static void main(String[] args) {
        // write your code here
        new Main().run();

    }

    public void run() {
        latch = new CountDownLatch(1);
        aLong = new AtomicLong(Long.MAX_VALUE);
        start = new AtomicLong(System.nanoTime());
        cars = new Car[10];
        for (int i = 0; i < 10; i++) {
            cars[i] = new Car(i + 1);
            cars[i].start();
        }
        latch.countDown();
        while (Thread.activeCount() != 2){
        }
        System.out.println("The worst time is " + (aLong.get() - start.get()));
    }

    class Car extends Thread {
        private int n;

        Car(int n) {
            this.n = n;
        }

        public void run() {
            try {
                latch.await();
                for (int i = 1; i <= 20; i++)
                    System.out.println("Car №" + this.n + " at Lap №" + i);
                long cur = System.nanoTime();
                long worst = aLong.get();
                while (cur > worst && !aLong.compareAndSet(worst, cur)){
                    worst = aLong.get();
                }
                System.out.println("Car №" + this.n + " finished!");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
