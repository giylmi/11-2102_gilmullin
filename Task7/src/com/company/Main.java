package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        ArrayList<Student> list = new ArrayList<Student>();
        BufferedReader br = new BufferedReader(new FileReader("input.txt"));
        PrintWriter pw = new PrintWriter("output.txt");
        String s = br.readLine();
        while (s != null){
            String[] strings = s.split(" ");
            String name = strings[0];
            int score = Integer.valueOf(strings[1]);
            list.add(new Student(score, name));
            s = br.readLine();
        }
        Collections.sort(list);
        int i = 0;
        for (Student student: list){
            if (i++ < 10){
                pw.println(student.getName());
            }
            else{
                break;
            }
        }
        pw.close();
    }
}
class Student implements Comparable<Student>{
    private int score;
    private String name;

    Student(int score, String name) {
        this.score = score;
        this.name = name;
    }

    int getScore() {
        return score;
    }

    void setScore(int score) {
        this.score = score;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Student o) {
	/*TODO если учесть, что баллы могут быть только положительными, то можно сократить до return o.score - this.score;*/
        if (this.score >= o.score)
            return -1;
        return 1;
    }
}
