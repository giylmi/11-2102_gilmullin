package com.company;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Main {
    CyclicBarrier barrier;
    int count[] = new int[3];

    public static void main(String[] args) {
	// write your code here
        new Main().run();
    }

    public void run(){
        barrier = new CyclicBarrier(3, new Runnable() {
            @Override
            public void run() {
                int sum = 0;
                for (int i: count)
                    sum += i;
                System.out.println(sum);
            }
        });
        new Thread(new Task(0, 300000, 0)).start();
        new Thread(new Task(300000, 600000, 1)).start();
        new Thread(new Task(600000, 1000000, 2)).start();
    }

    class Task implements Runnable {
        private int start, end, num;

        Task(int start, int end, int num) {
            this.start = start;
            this.end = end;
            this.num = num;
        }

        @Override
        public void run() {
            int count1 = 0;
            for (int i = start; i < end; i++) {
                if (isHappy(i))
                    count1++;
            }
            System.out.println("Task " + num + " finished counting! Now waiting for barrier!");
            try {
                count[num] = count1;
                barrier.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }

        }

        public boolean isHappy(int n) {
            int s = 0;
            for (int i = 0; i < 6; i++) {
                if (i < 3)
                    s += n % 10;
                else
                    s -= n % 10;
                n /= 10;
            }
            return s == 0;
        }
    }
}
