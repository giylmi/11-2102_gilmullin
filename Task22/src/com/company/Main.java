package com.company;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) throws SQLException {
	// write your code here
        Connection con = ConnectionFactory.getInstance().getConnection();
        Statement stmt = con.createStatement();
        String query = "";
        ResultSet rs = null;

        System.out.println("Выбрать всех авторов, чьи фамилии начинаются на 'M' или на 'K'");
        query = "SELECT name FROM author";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getString(1));
            while(rs.next())
                System.out.println(rs.getString(1));
        }
        else
            System.out.println("Empty result");

        System.out.println("Выбрать все книги, у которых цена больше 100");
        query = "SELECT name FROM book WHERE price > 100";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getString(1));
            while(rs.next())
                System.out.println(rs.getString(1));
        }
        else
            System.out.println("Empty result");

        //System.out.println("Вывести список названий книг и имен ее авторов");

        System.out.println("Найти книгу с минимальной ценой.");
        query = "SELECT name FROM book WHERE price = (SELECT MIN(price) FROM book)";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getString(1));
            while(rs.next())
                System.out.println(rs.getString(1));
        }
        else
            System.out.println("Empty result");

        System.out.println("Посчитать среднюю стоимость книг");
        query = "SELECT AVG(price) FROM book";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getInt(1));
        }
        else
            System.out.println("Empty result");

        System.out.println("Найти общее количество авторов.");
        query = "SELECT COUNT(*) FROM author";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getInt(1));
        }
        else
            System.out.println("Empty result");

        System.out.println("Найти максимальную стоимость книги для каждого автора");
        query = "SELECT MAX(price) FROM book GROUP BY author_id";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getInt(1));
            while(rs.next())
                System.out.println(rs.getInt(1));
        }
        else
            System.out.println("Empty result");

        System.out.println("Вывести количество книг по каждому названию книги.");
        query = "SELECT COUNT(*) FROM book GROUP BY name";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getInt(1));
            while(rs.next())
                System.out.println(rs.getInt(1));
        }
        else
            System.out.println("Empty result");

        System.out.println("Сколько групп однофамильцев встречается среди авторов.");
        query = "SELECT COUNT(*) FROM (SELECT name FROM author GROUP BY name HAVING COUNT(*) > 1) as foo";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getInt(1));
            while(rs.next())
                System.out.println(rs.getInt(1));
        }
        else
            System.out.println("Empty result");

        System.out.println("Выбрать авторов у которых средняя стоимость книги больше 100.");
        query = "SELECT name FROM author WHERE (select AVG(price) from book WHERE author.id = book.author_id) > 100";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getString(1));
            while(rs.next())
                System.out.println(rs.getString(1));
        }
        else
            System.out.println("Empty result");

        System.out.println("Выбрать всех авторов, у которых больше одной книги.");
        query = "SELECT name FROM author WHERE (select COUNT(*) from book WHERE author.id = book.author_id) > 1";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getString(1));
            while(rs.next())
                System.out.println(rs.getString(1));
        }
        else
            System.out.println("Empty result");

        System.out.println("Выбрать автора самой дорогой книги.");
	/*TODO можно проще, используй выборку из двух таблиц в основном запросе*/
        query = "SELECT name FROM author WHERE id = (select author_id from book WHERE price = (select MAX(price) from book))";
        rs = stmt.executeQuery(query);
        if (rs.next()){
            System.out.println(rs.getString(1));
            while(rs.next())
                System.out.println(rs.getString(1));
        }
        else
            System.out.println("Empty result");
    }
}
