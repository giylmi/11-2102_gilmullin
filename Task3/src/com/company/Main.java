package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader br = new BufferedReader(new FileReader("input.txt"));
        PrintWriter pw = new PrintWriter("output.txt");
        String s = br.readLine();
        HashSet<Student> students = new HashSet<Student>();
        while(s != null){
            String name = s;
            String lastName = br.readLine();
            String age = br.readLine();
            students.add(new Student(name, lastName, age));
            s = br.readLine();
        }
        for (Student student: students){
            pw.println(student.toString());
        }
        pw.close();
    }


}
class Student{
    private String name;
    private String lastName;
    private String age;

    Student(String name, String lastName, String age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (!age.equals(student.age)) return false;
        if (!lastName.equals(student.lastName)) return false;
        if (!name.equals(student.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        return result;
    }

    public String toString(){
        return "[\nname: " + name + "\nfirstName: " + lastName + "\nage: " + age + "\n]";
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getLastName() {
        return lastName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    String getAge() {
        return age;
    }

    void setAge(String age) {
        this.age = age;
    }
}
