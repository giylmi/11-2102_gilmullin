package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by adel on 10.12.13.
 */
public class Server {

    public void initServer() throws IOException {
        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(12345);
        } catch (IOException e) {
            System.err.println("Could not listen on port: 12345.");
            System.exit(1);
        }
        Socket clientSocket = null;
        System.out.println("Waiting for connection.....");

        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            System.err.println("Accept failed.");
            System.exit(1);
        }

        System.out.println("Connection successful");
        System.out.println("Waiting for input.....");

        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),
                true);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));

        String inputLine;

        int balance = 0;
        while ((inputLine = in.readLine()) != null) {
            System.out.println("Server: " + inputLine);

            if (inputLine.equals("?")) {
                out.println("\"q.\" ends Client\n\"show\" shows your account balance\n\"take N\" gives you N bucks if possible\n\"put N\" puts N bucks to your account! BUT! DO NOT CHEAT!");
                out.println("#end");
            } else if (inputLine.equals("q")) {
                out.println("Have a good day!");
                out.println("#end");
                break;
            } else if (inputLine.equals("show")) {
                out.println(balance);
                out.println("#end");
                break;
            } else if (inputLine.split(" ").length != 2) {
                out.println("Bad request:( type \"?\" to know allowed commands");
                out.println("#end");
            } else if (inputLine.split(" ")[0].equals("take")) {
                int want = Integer.valueOf(inputLine.split(" ")[1]);
                if (want > balance)
                    out.println("You do not have such amount of money at your account");
                else {
                    balance -= want;
                    out.println("Take your money!");
                }
                out.println("#end");
            } else if (inputLine.split(" ")[0].equals("put")) {
                balance += Integer.valueOf(inputLine.split(" ")[1]);
                out.println("Money is taken!");
                out.println("#end");
            } else {
                out.println("Bad request:( type \"?\" to know allowed commands");
                out.println("#end");
            }
        }

        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server();
        server.initServer();
    }
}
