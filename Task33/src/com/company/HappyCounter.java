package com.company;

import java.util.concurrent.atomic.AtomicInteger;

public class HappyCounter {
    AtomicInteger count;

    public static void main(String[] args) {
        // write your code here
        new HappyCounter().run();
    }

    private void run() {
        count = new AtomicInteger();

        new Thread(new Task(0, 300000)).start();
        new Thread(new Task(300000, 600000)).start();
        new Thread(new Task(600000, 1000000)).start();
//        new Thread(new Task(0, 1002)).start();

        while (Thread.activeCount() != 2) {
        }
        System.out.println(count);
    }

    class Task implements Runnable {
        private int start, end;

        Task(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public void run() {
            for (int i = start; i < end; i++) {
                if (isHappy(i))
                    count.incrementAndGet();
            }
        }

        public boolean isHappy(int n) {
            int s = 0;
            for (int i = 0; i < 6; i++) {
                if (i < 3)
                    s += n % 10;
                else
                    s -= n % 10;
                n /= 10;
            }
            return s == 0;
        }
    }
}
