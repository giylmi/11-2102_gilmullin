package com.company;

import org.omg.PortableInterceptor.INACTIVE;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by adel on 10.12.13.
 */
public class ExecutorServiceHappyCounter implements Callable<Integer> {
    private int start, end;

    public ExecutorServiceHappyCounter(int start, int end) {
        this.start = start;
        this.end = end;
    }

    private boolean isHappy(int n) {
        int s = 0;
        for (int i = 0; i < 6; i++) {
            if (i < 3)
                s += n % 10;
            else
                s -= n % 10;
            n /= 10;
        }
        return s == 0;
    }

    @Override
    public Integer call() throws Exception {
        int count = 0;
        for (int i = start; i < end; i++) {
            if (isHappy(i))
                count++;
        }
        return count;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(3);
        List<Future<Integer>> list = new ArrayList<Future<Integer>>();
        Future<Integer> future = service.submit(new ExecutorServiceHappyCounter(0, 300000));
        list.add(future);
        future = service.submit(new ExecutorServiceHappyCounter(300000, 600000));
        list.add(future);
        future = service.submit(new ExecutorServiceHappyCounter(600000, 1000000));
        list.add(future);
        int sum = 0;
        for (Future<Integer> future1: list) sum += future1.get();
        System.out.println(sum);
        service.shutdown();
    }
}
