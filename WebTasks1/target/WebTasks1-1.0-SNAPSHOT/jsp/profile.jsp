<%--
  Created by IntelliJ IDEA.
  User: adel
  Date: 11.12.13
  Time: 11:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Profile</title>
    <link href="/css/style.css" rel="stylesheet">
</head>
<body>
<h1>StudentBook ITIS</h1><br>

<a href="/edit?id=${user.id}">edit</a>
<a href="/gallery">Gallery</a>
<div id="left">
    <img src="${user.ava}" class="ava">
    <p><img src="/images/phone.jpg" class="pic">${user.phone}</p>
    <p><img src="/images/twitter.png" class="pic">${user.twitter}</p>
    <p><img src="/images/mail.jpg" class="pic">${user.mail}</p>
</div>
<div id="right">
    <p><h3>${user.name}</h3></p>
    <p>Date of birth: ${user.birth}</p>
    <p>Lab: ${user.lab}</p>
    <p>Addition:</p>
    <ul>
        <li>Good ${user.gender}</li>
        <li>Wants to win!</li>
    </ul>
</div>
</body>
</html>