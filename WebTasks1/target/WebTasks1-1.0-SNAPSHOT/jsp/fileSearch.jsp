<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>search</title>
</head>
<body>
    <form method="get" action="/find">
        <p>Enter directory to search from: <input type="text" name="dir" required="true"></p>
        <p>Enter the file to search: <input type="text" name="file" required="true"></p>
        <p><button type="submit">Find!</button></p>
    </form>
    <c:if test="${results != null}">
        <h3>Search results</h3>
        <c:forEach items="${results}" var="result">
            <p>${result}</p>
        </c:forEach>
    </c:if>
</body>
</html>