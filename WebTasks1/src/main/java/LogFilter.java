import javax.servlet.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

/**
 * Created by adel on 11.12.13.
 */
public class LogFilter implements Filter {
    PrintWriter pw;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        try {
            pw = new PrintWriter("log.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        pw.println(servletRequest.toString() + " " + servletRequest.getParameter("id") + " " + new Date().getTime());
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        pw.close();
    }
}
