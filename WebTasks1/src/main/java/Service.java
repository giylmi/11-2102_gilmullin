import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by adel on 11.12.13.
 */
public class Service {
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet rs = null;

    private Connection getConnection() throws SQLException {
        return ConnectionFactory.getInstance().getConnection();
    }

    public User getUser(int id){
        User user = new User();
        try{
            String querystring = "SELECT * FROM users WHERE id = ?";
            connection = getConnection();
            ptmt = connection.prepareStatement(querystring);
            ptmt.setInt(1, id);
            rs = ptmt.executeQuery();
            if (rs.next()){
                user.setId(rs.getInt("id"));
                user.setAva(rs.getString("ava"));
                user.setBirth(rs.getString("birth"));
                user.setGender(rs.getString("gender"));
                user.setLab(rs.getString("lab"));
                user.setMail(rs.getString("mail"));
                user.setTwitter(rs.getString("twitter"));
                user.setPhone(rs.getString("phone"));
                user.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return user;
        }
    }
    public void addCredentials(String name, String pass){
        try{
            String query = "INSERT INTO credentials (name, password, salt) VALUES (?, ?, ?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(query);
            ptmt.setString(1, name);
            ptmt.setString(2, md5(pass + name));
            ptmt.setString(3, name);
            ptmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public int checkAuth(String name, String pass) {
        int answer = 0;
        try {
            String querystring = "SELECT id, password, salt FROM credentials WHERE name = ?";
            connection = getConnection();
            ptmt = connection.prepareStatement(querystring);
            ptmt.setString(1, name);
            rs = ptmt.executeQuery();
            if (rs.next() && rs.getString(2).equals(md5(pass + rs.getString(3))))
                answer = rs.getInt(1);
            else
                answer = -1;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return answer;
        }
    }

    public static String md5(String input) {

        String md5 = null;

        if(null == input) return null;

        try {

            //Create MessageDigest object for MD5
            MessageDigest digest = MessageDigest.getInstance("MD5");

            //Update input string in message digest
            digest.update(input.getBytes(), 0, input.length());

            //Converts message digest value in base 16 (hex)
            md5 = new BigInteger(1, digest.digest()).toString(16);

        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        }
        return md5;
    }

    public void update(User user) {
        try{
            String query = "UPDATE users SET birth=?, gender=?, lab=?, mail=?, twitter=?, phone=?, name=? WHERE id=?";           connection = getConnection();
            ptmt = connection.prepareStatement(query);
            ptmt.setString(1, user.getBirth());
            ptmt.setString(2, user.getGender());
            ptmt.setString(3, user.getLab());
            ptmt.setString(4, user.getMail());
            ptmt.setString(5, user.getTwitter());
            ptmt.setString(6, user.getPhone());
            ptmt.setString(7, user.getName());
            ptmt.setInt(8, user.getId());
            ptmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void addMsg(String msg){
        try{
            String query = "INSERT INTO messages (message) VALUES (?)";
            ptmt = connection.prepareStatement(query);
            ptmt.setString(1, msg);
            ptmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try{
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<User> findAll(){
        User user = new User();
        ArrayList<User> list = new ArrayList<User>();
        try{
            String querystring = "SELECT * FROM users";
            connection = getConnection();
            ptmt = connection.prepareStatement(querystring);
            rs = ptmt.executeQuery();
            while (rs.next()){
                user = new User();
                user.setId(rs.getInt("id"));
                user.setAva(rs.getString("ava"));
                list.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return list;
        }
    }
}
