import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by adel on 11.12.13.
 */
public class LoginServlet extends HttpServlet {
    private Service service;
    public LoginServlet(){
        service = new Service();
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("login");
        String pass = request.getParameter("password");

        int id = service.checkAuth(name, pass);
        if (id != -1){
            request.setAttribute("user", service.getUser(id));
            getServletConfig().getServletContext().getRequestDispatcher("/jsp/profile.jsp").forward(request, response);
        }
        else{
            request.setAttribute("error", true);
            getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}
