import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by adel on 11.12.13.
 */
public class EditServlet extends HttpServlet {
    Service service;
    public EditServlet(){
        service = new Service();
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = service.getUser(Integer.valueOf(request.getParameter("id")));
        request.getSession().setAttribute("user", user);
        getServletConfig().getServletContext().getRequestDispatcher("/jsp/edit.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User)request.getSession().getAttribute("user");
        user.setBirth(request.getParameter("birth"));
        user.setGender(request.getParameter("gender"));
        user.setLab(request.getParameter("lab"));
        user.setMail(request.getParameter("mail"));
        user.setTwitter(request.getParameter("twitter"));
        user.setPhone(request.getParameter("phone"));
        user.setName(request.getParameter("name"));
        service.update(user);
        getServletConfig().getServletContext().getRequestDispatcher("/jsp/profile.jsp").forward(request, response);
    }
}
