import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by adel on 11.12.13.
 */
public class GalleryServlet extends HttpServlet {
    Service service;
    public GalleryServlet(){
        service = new Service();
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<User> list = service.findAll();
        request.setAttribute("list", list);
        getServletConfig().getServletContext().getRequestDispatcher("/jsp/gallery.jsp").forward(request, response);
    }
}
