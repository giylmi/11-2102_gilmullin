import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by adel on 11.12.13.
 */
public class SupportServlet extends HttpServlet {
    ArrayBlockingQueue<Message> queue;
    Service service;

    public SupportServlet(){
        service = new Service();
        queue = new ArrayBlockingQueue<Message>(10);
        new Thread(new Consumer()).start();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response){
        Message msg = new Message(request.getParameter("message"));
        try {
            queue.put(msg);
            getServletConfig().getServletContext().getRequestDispatcher("/jsp/done.sjp").forward(request, response);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class Message{
        private String msg;

        Message(String str){
            this.msg=str;
        }

        public String getMsg() {
            return msg;
        }
    }

    class Consumer implements Runnable{

        @Override
        public void run() {
            try{
                String msg;
                //consuming messages until exit message is received
                while(true){
                    msg = queue.take().getMsg();
                    service.addMsg(msg);
                }
            }catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
