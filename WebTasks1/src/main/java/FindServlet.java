import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by adel on 11.12.13.
 */
public class FindServlet extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<String> result = new ArrayList<String>();
        File current = new File(request.getParameter("dir"));

        String toFind = request.getParameter("file");

        search(result, current, toFind);
        request.setAttribute("results", result);
        getServletConfig().getServletContext().getRequestDispatcher("/jsp/fileSearch.jsp").forward(request, response);
    }

    public void search(ArrayList<String> result, File file, String s){
        if (file.isFile()){
            if (file.getName().contains(s)){
                result.add(file.getAbsolutePath());
            }
            return;
        }
        File[] list = file.listFiles();
        if (list != null)
        for (File file1: list){
            search(result, file1, s);
        }
    }
}
