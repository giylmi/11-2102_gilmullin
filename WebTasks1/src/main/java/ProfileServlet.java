import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by adel on 11.12.13.
 */
public class ProfileServlet extends HttpServlet {
    Service service;

    public ProfileServlet(){
        service = new Service();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("user", service.getUser(Integer.valueOf(request.getParameter("id"))));
        getServletConfig().getServletContext().getRequestDispatcher("/jsp/profile.jsp").forward(request, response);
    }
}
