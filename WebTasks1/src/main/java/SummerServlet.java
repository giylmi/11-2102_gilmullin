import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by adel on 11.12.13.
 */
public class SummerServlet extends HttpServlet {
    AtomicInteger first, second, sum;
    public SummerServlet(){
        first = new AtomicInteger();
        second = new AtomicInteger();
        sum = new AtomicInteger();
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int f1 = Integer.valueOf(request.getParameter("first")), s1 = Integer.valueOf(request.getParameter("second"));
        if (first.get() == f1 && second.get() == s1)
            request.setAttribute("answer", sum.get());
        else{
            synchronized(this){
                first.set(f1);
                second.set(s1);
                sum.set(first.get() + second.get());
                request.setAttribute("answer", sum.get());
            }
        }
        getServletConfig().getServletContext().getRequestDispatcher("/jsp/summer.jsp").forward(request, response);
    }
}
