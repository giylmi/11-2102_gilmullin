<%--
  Created by IntelliJ IDEA.
  User: adel
  Date: 11.12.13
  Time: 10:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>summer</title>
</head>
<body>
    <form action="/sum" method="get">
        <input type="number" required="true" name="first">
        +
        <input type="number" required="true" name="second">
        <button type="submit">Submit</button>
    </form>
    <c:if test="${answer != null}">
        <p>${answer}</p>
    </c:if>
</body>
</html>
