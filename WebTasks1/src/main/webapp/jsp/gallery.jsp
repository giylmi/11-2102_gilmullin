<%--
  Created by IntelliJ IDEA.
  User: adel
  Date: 11.12.13
  Time: 23:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Gallery</title>
    <link href="/css/style.css" rel="stylesheet">
</head>
<body>
    <c:forEach items="${list}" var="item">
        <a href="/profile?id=${item.id}"><img src="${item.ava}" class="ava"></a>
    </c:forEach>
</body>
</html>
