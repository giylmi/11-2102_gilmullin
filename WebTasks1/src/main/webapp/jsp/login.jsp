<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>login</title>
</head>
<body>
<form method="get" action="auth">
    <c:if test="${error}">
        Wrong pair {login, password}
    </c:if>
    <p>Enter login: <input type="text" name="login"></p>
    <p>Enter password: <input type="password" name="password"></p>
    <p><input type="submit" value="login"></p>
</form>
</body>
</html>