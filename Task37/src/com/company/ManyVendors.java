package com.company;

import java.util.Random;
/*TODO пока один покупатель покупает бананы, другой не может купить апельсины, т.к. ждет на том же локе.*/
public class ManyVendors {

    volatile int banana, orange, peach;
    Random random;

    public static void main(String[] args) {
        // write your code here
        new ManyVendors().run();
    }

    private void run() {
        random = new Random();

        Supplier supplier = new Supplier();
        supplier.start();

        for (int i = 0; i < 100; i++) {
            new Customer("Customer " + i).start();
        }
    }

    public synchronized void giveBanana(int amount) {
        if (amount > banana) {
            System.out.println("No bananas, sorry, sir!");
        } else {
            System.out.println("Here are your bananas, sir!");
            banana -= amount;
        }
    }

    public synchronized void giveOrange(int amount) {
        if (amount > orange) {
            System.out.println("No oranges, sorry, sir!");
        } else {
            System.out.println("Here are your oranges, sir!");
            orange -= amount;
        }
    }

    public synchronized void givePeach(int amount) {
        if (amount > peach) {
            System.out.println("No peaches, sorry, sir!");
        } else {
            System.out.println("Here are your peaches, sir!");
            peach -= amount;
        }
    }

    class Customer extends Thread {
        public Customer(String s) {
            super(s);
        }

        public void run() {
            switch (random.nextInt(3)) {
                case 0: {
                    System.out.println(this.getName() + " wants bananas");
                    giveBanana(random.nextInt(100));
                    break;
                }
                case 1: {
                    System.out.println(this.getName() + " wants oranges");
                    giveOrange(random.nextInt(100));
                    break;
                }
                case 2: {
                    System.out.println(this.getName() + " wants peaches");
                    givePeach(random.nextInt(100));
                    break;
                }
            }
        }
    }

    class Supplier extends Thread {

        public void run() {
            try {
                for (int i = 0; i < 5; i++) {
                    banana += 1000;
                    System.out.println("Some more bananas supplied!");
                    orange += 1000;
                    System.out.println("Some more oranges supplied!");
                    peach += 1000;
                    System.out.println("Some more peaches supplied!");
                    Thread.sleep(100);
                }
                System.out.println("Supplier goes home!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
