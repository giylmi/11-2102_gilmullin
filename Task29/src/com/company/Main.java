package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        for(int i = 1; i <= 100; i++){
            Car car = new Car(i);
            car.start();
        }
    }
}
class Car extends Thread{
    private int n;

    Car(int n) {
        this.n = n;
    }

    public void run(){
        for (int i = 1; i <= 20; i++)
            System.out.println("Car №" + this.n + " at Lap №" + i);
        System.out.println("Car №" + this.n + " finished!");
    }
}
