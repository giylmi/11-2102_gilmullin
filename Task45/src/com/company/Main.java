package com.company;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    CountDownLatch latch;
    AtomicBoolean[] list;
    Random random;
    AtomicInteger sold;

    public static void main(String[] args) {
        // write your code here
        new Main().run();
    }

    public void run(){
        latch = new CountDownLatch(1);
        list = new AtomicBoolean[10];
        for (int i = 0; i < 10; i++)
            list[i] = new AtomicBoolean(false);
        random = new Random();
        sold = new AtomicInteger();
        for (int i = 0; i < 20; i++){
            new Spectator("Spectator " + i).start();
        }
        latch.countDown();
    }
    class Spectator extends Thread{
        public Spectator(String s) {
            super(s);
        }

        public void run(){
            try {
                latch.await();
                int t = random.nextInt(10);
                while (!list[t].compareAndSet(false, true)){
                    t = random.nextInt(10);
                    if (sold.get() == 10){
                        System.out.println(this.getName() + " doesn't go to movie!");
                        return;
                    }
                }
                sold.incrementAndGet();
                System.out.println(this.getName() + " enjoys the show at place " + t);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
