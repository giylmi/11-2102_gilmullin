package com.company;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    int money;
    public static void main(String[] args) {
	// write your code here
        new Main().run();
    }

    public void run(){
        money = 1000;
        Random random = new Random();
        for (int i = 0; i < 20; i++){
            new User("User " + i, random.nextInt(150) + 50).start();
        }
    }

    synchronized boolean take(int money){
        if (this.money < money)
            return false;
        this.money -= money;
        return true;
    }

    class User extends Thread{
        private int want;

        User(String name, int want) {
            super(name);
            this.want = want;
        }

        public void run(){
            if (take(want))
                System.out.println(this.getName() + " got his money!");
            else
                System.out.println(this.getName() + "'s pockets are empty!");
        }
    }
}
