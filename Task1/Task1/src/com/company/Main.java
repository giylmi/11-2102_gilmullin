package com.company;


import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

import static java.lang.System.exit;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);

        String initialDir = sc.nextLine();

        if (initialDir == null || initialDir.equals("")){
            System.out.println("Error: Empty line");
            exit(0);
        }

        File current = new File(initialDir);

        String toFind = sc.nextLine();

        if (toFind == null || toFind.equals("")){
            System.out.println("Error: Empty line");
            exit(0);
        }

        search(current, toFind);
    }

    public static void search(File file, String s){
        if (file.isFile()){
            if (file.getName().contains(s)){
                System.out.println(file.getAbsolutePath());
            }
            return;
        }
        File[] list = file.listFiles();
        if (list != null)
        for (File file1: list){
            search(file1, s);
        }
    }
}
