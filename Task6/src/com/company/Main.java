package com.company;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter start charset: ");
        String charset = sc.next();
        if (Charset.isSupported(charset)){
            InputStreamReader isr = new InputStreamReader(new FileInputStream("input.txt"), Charset.forName(charset));
            System.out.println("Enter final charset: ");
            charset = sc.next();
            if (Charset.isSupported(charset)){
                OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("output.txt"), Charset.forName(charset));
                while(isr.ready())
                    osw.write(isr.read());
                osw.close();
            }
            else{
                System.out.println("Charset is not supported!");
            }
        }
        else{
            System.out.println("Charset is not supported!");
        }
    }
}
