package com.company;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
	// write your code here
        Counter counter = new Counter("out.txt");
        Counter counter1 = new Counter("out1.txt");
        Counter counter2 = new Counter("out2.txt");

        counter.start();
        counter1.start();
        counter2.start();
    }
}

class Counter extends Thread{
    private String filename;
    private LineNumberReader reader;

    Counter(String filename) throws FileNotFoundException {
        this.filename = filename;
        this.reader = new LineNumberReader(new FileReader(filename));
    }

    public void run(){
        int count = 0;
        try {
            while (reader.readLine() != null) count++;
            System.out.println(count + " lines in " + filename);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}