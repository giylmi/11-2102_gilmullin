package com.company;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class Summer extends RecursiveTask<Integer> {

    private static final int SEQUENTIAL_THRESHOLD = 5;

    private final int[] data;
    private final int start;
    private final int end;

    public Summer(int[] data, int start, int end) {
        this.data = data;
        this.start = start;
        this.end = end;
    }

    public Summer(int[] data) {
        this(data, 0, data.length);
    }

    @Override
    protected Integer compute() {
        final int length = end - start;
        if (length < SEQUENTIAL_THRESHOLD) {
            return computeDirectly();
        }
        final int split = length / 2;
        final Summer left = new Summer(data, start, start + split);
        final Summer right = new Summer(data, start + split, end);

        ForkJoinTask.invokeAll(left, right);
        return (right.join() + left.join());


    }

    private Integer computeDirectly() {
        System.out.println(Thread.currentThread() + " computing: " + start
                + " to " + end);
        int sum = 0;
        for (int i = start; i < end; i++) {
                sum += data[i];
        }
        return sum;
    }

    public static void main(String[] args) {
        // create a random data set
        final int[] data = new int[1000];
        final Random random = new Random();
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(100);
        }

        // submit the task to the pool
        final ForkJoinPool pool = new ForkJoinPool(4);
        final Summer finder = new Summer(data);
        System.out.println(pool.invoke(finder));
    }
}