package com.company;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Main {
    CyclicBarrier barrier;
    ArrayBlockingQueue<String> list;

    public static void main(String[] args) {
	// write your code here
        new Main().run();
    }

    public void run(){
        barrier = new CyclicBarrier(10, new Runnable() {
            @Override
            public void run() {
                System.out.println("Ready to go!");
                for (String s: list)
                    System.out.println(s);
                list.clear();
            }
        });
        list = new ArrayBlockingQueue<String>(10);

        for (int i = 0; i < 20; i++){
            Thread thread = new Thread("Message " + i){
                public void run(){
                    try {
                        while (!list.offer(this.getName())){}
                        barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }  catch (IllegalStateException e) {
                        System.out.println("Queue is Full");
                    }
                }
            };
            thread.start();
        }

    }
}
