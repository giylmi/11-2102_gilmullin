package com.company;

import java.util.concurrent.CountDownLatch;

public class Main {
    CountDownLatch latch;

    public static void main(String[] args) {
	// write your code here
        new Main().run();
    }

    public void run(){
        latch = new CountDownLatch(10);
        for (int i = 0; i < 20; i++){
            new Spectator("Spectator " + i).start();
        }
    }
    class Spectator extends Thread{
        public Spectator(String s) {
            super(s);
        }

        public void run(){
            System.out.println(this.getName() + " is ready!");
            try {
                latch.countDown();
                latch.await();
                System.out.println(this.getName() + " enjoys the show!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
