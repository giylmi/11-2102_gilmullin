package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static java.lang.System.exit;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader br = new BufferedReader(new FileReader("input.txt"));
        int n = Integer.valueOf(br.readLine());
        int a[][] = new int[n][n];
        int row[] = new int[n];
        int col[] = new int[n];
        int l = 0;
        int r = 0;
        for (int i = 0; i < n; i++){
            String[] ss = br.readLine().split(" ");
            for (int j = 0; j < n; j++){
                a[i][j] = Integer.valueOf(ss[j]);
                row[i] += a[i][j];
                col[j] += a[i][j];
                if (i == j)
                    l += a[i][j];
                if (i + j == n - 1)
                    r += a[i][j];
            }
        }
        int need = l;
        if (need == r){
            for (int i = 0; i < n; i++)
                if (need != row[i] || need != col[i]){
                    System.out.println("Not magic");
                    exit(0);
                }
        }
        else {
            System.out.println("Not magic");
            exit(0);
        }
        System.out.println("Wow! It is MAGIC!");
    }


}
