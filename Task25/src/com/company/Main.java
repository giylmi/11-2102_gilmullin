package com.company;

import java.sql.*;

public class Main {

    public static void main(String[] args) throws SQLException {
        // write your code here
        Connection connection = ConnectionFactory.getInstance().getConnection();

        try {
            connection.setAutoCommit(false);

            String query;
            PreparedStatement statement = null;
            for (int i = 0; i < 2; i++) {
                query = "INSERT INTO book(author_id, name, price) VALUES (?, ?, ?)";
                statement = connection.prepareStatement(query);
                statement.setInt(1, i + 1);
                statement.setString(2, "New book " + i);
                statement.setInt(3, 150);
                statement.execute();
            }
/*TODO это две транзакции, а требовалось сделать в одну. Используй savepoint http://docs.oracle.com/javase/tutorial/jdbc/basics/transactions.html*/
            connection.commit();

            query = "SELECT COUNT(*) FROM book";
            statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();
            rs.next();
            if (rs.getInt(1) > 10){
                query = "UPDATE book SET price = (price * 0.9)";
            }
            //if (true){ throw new Exception();}

            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            if (connection != null)
                connection.close();
        }
    }
}
