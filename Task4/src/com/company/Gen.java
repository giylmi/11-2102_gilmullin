package com.company;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;

public class Gen {

    public static void main(String[] args) throws FileNotFoundException {
	// write your code here
        PrintWriter pw = new PrintWriter("out.txt");
        Random random = new Random();
        for(int i = 0; i < 100000; i++)
            pw.print((random.nextInt(500) + 1) + " ");
        pw.close();
    }
}
