package com.company;

import java.io.*;
import java.util.HashSet;
import java.util.Random;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA.
 * User: adel
 * Date: 12/9/13
 * Time: 2:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class BinaryTreeTest {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("out.txt"));
        StreamTokenizer st = new StreamTokenizer(br);
        PrintWriter pw = new PrintWriter("result.txt");
        Random random = new Random();

        TreeSet<Integer> tree = new TreeSet<Integer>();
        HashSet<Integer> hash = new HashSet<Integer>();
        for (int i = 0; i < 100000; i++){
            st.nextToken();
            tree.add((int)st.nval);
            hash.add((int)st.nval);
        }

        for (int i = 0; i < 5; i++){
            int f = random.nextInt(500) + 1;
            long start;
            pw.println("Test " + i + " find " + f + ":");
            start = System.nanoTime();
            tree.contains(f);
            pw.println("TreeSet: " + (System.nanoTime() - start));
            start = System.nanoTime();
            hash.contains(f);
            pw.println("HashSet: " + (System.nanoTime() - start));
        }
        pw.close();
    }
}
