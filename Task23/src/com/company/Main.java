package com.company;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) throws SQLException {
        // write your code here
        Connection connection = ConnectionFactory.getInstance().getConnection();


        try {
            connection.setAutoCommit(false);

            String query = "UPDATE book SET price = (price * 1.05)";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
            //if (true){ throw new Exception();}

            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            if (connection != null)
                connection.close();
        }

    }
}
