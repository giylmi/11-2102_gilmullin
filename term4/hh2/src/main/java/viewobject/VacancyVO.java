package viewobject;

/**
 * Created by adel on 24.04.14.
 */
public class VacancyVO {
    private Long id;
    private String title;

    public VacancyVO() {
    }

    public VacancyVO(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
