package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import service.CompanyService;

/**
 * Created by adel on 23.04.14.
 */
@Controller
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @RequestMapping("/company/{id}")
    public ModelAndView getCompany(@PathVariable Long id){
        return new ModelAndView("company_profile", "company", companyService.getCompanyById(id));
    }
}
