package controller.api;

import model.Vacancy;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.CompanyService;
import viewobject.VacancyVO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adel on 27.04.14.
 */
@RestController
@RequestMapping("/api/vacancy")
public class VacancyRestController {

    @Autowired
    CompanyService companyService;

    @RequestMapping("/list")
    public ResponseEntity<List<VacancyVO>> findAll(){
        Iterable<Vacancy> vacancies = companyService.getAllVacancies();
        List<VacancyVO> vacancyVOs = new ArrayList<VacancyVO>();
        for (Vacancy vacancy: vacancies) vacancyVOs.add(new DozerBeanMapper().map(vacancy, VacancyVO.class));
        return new ResponseEntity<List<VacancyVO>>(vacancyVOs, HttpStatus.OK);
    }

}
