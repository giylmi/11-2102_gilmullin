package controller.api;

import model.Company;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.CompanyService;
import viewobject.CompanyVO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adel on 27.04.14.
 */
@RestController
@RequestMapping("/api/company/")
public class CompanyRestController {

    @Autowired
    private CompanyService companyService;

    @RequestMapping("/list")
    public ResponseEntity<List<CompanyVO>> findAll(){
        Iterable<Company> companies = companyService.findAll();
        List<CompanyVO> companyVOs = new ArrayList<CompanyVO>();
        for (Company company: companies) companyVOs.add(new DozerBeanMapper().map(company, CompanyVO.class));
        return new ResponseEntity<List<CompanyVO>>(companyVOs, HttpStatus.OK);
    }
}
