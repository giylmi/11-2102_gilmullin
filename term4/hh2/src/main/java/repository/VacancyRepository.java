package repository;

import model.CV;
import model.Vacancy;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface VacancyRepository extends CrudRepository<Vacancy, Long> {

    @Transactional
    @Query("select vacancy from Vacancy vacancy join vacancy.category c where c.id = ?1")
        //      SELECT * FROM cv, cv_category WHERE ... AND cv_category.category_id=?1
    Iterable<Vacancy> findByCategory(Long categoryID);

    List<Vacancy> findByTitleStartingWithIgnoreCase(String title);
}
