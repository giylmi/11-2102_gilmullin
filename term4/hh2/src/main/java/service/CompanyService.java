package service;

import model.Company;
import model.Vacancy;

public interface CompanyService {
    Iterable<Company> findAll();
    Vacancy getVacancyById(Long id);
    Iterable<Vacancy> getAllVacancies();
    Company getCompanyById(Long id);
    Iterable<Vacancy> getVacancyListByCategory(Long categoryId);
    Iterable<Vacancy> getVacanciesByNamePart(String term);
    void saveVacancy(Vacancy vacancy);
}
