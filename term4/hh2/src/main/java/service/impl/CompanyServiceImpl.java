package service.impl;

import model.Company;
import model.Vacancy;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repository.CompanyRepository;
import repository.VacancyRepository;
import service.CompanyService;

import java.util.Iterator;

@Service
public class CompanyServiceImpl implements CompanyService{

    private CompanyRepository companyRepository;
    private VacancyRepository vacancyRepository;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository, VacancyRepository vacancyRepository) {
        this.companyRepository = companyRepository;
        this.vacancyRepository = vacancyRepository;
    }

    @Override
    public Iterable<Company> findAll() {
        return companyRepository.findAll();
    }

    @Override
    public Vacancy getVacancyById(Long id) {
        return vacancyRepository.findOne(id);
    }

    @Override
    public Iterable<Vacancy> getAllVacancies() {
        return vacancyRepository.findAll();
    }

    @Transactional
    @Override
    public Company getCompanyById(Long id) {
        Company company = companyRepository.findOne(id);
        Hibernate.initialize(company.getVacancies());
        return company;
    }

    @Override
    public Iterable<Vacancy> getVacancyListByCategory(Long categoryId) {
        return vacancyRepository.findByCategory(categoryId);
    }

    @Override
    public Iterable<Vacancy> getVacanciesByNamePart(String term) {
        return vacancyRepository.findByTitleStartingWithIgnoreCase(term);
    }

    @Override
    public void saveVacancy(Vacancy vacancy) {
        vacancyRepository.save(vacancy);
    }


}