package foo.bar;

import java.util.List;

/**
 * Created by adel on 08.02.14.
 */
public class Club {

    private List<Player> squad;
    private Owner owner;
    private Coach coach;
    private String name;
    private String ligue;

    public void printScheme(){
        System.out.println("Club: " + this.getName() + "\n" +
                           "Owner: " + this.getOwner().getName() + "\n" +
                           "Coach: " + this.getCoach().getName() + "\n" +
                           "Ligue: " + this.getLigue() + "\n" +
                           "Squad:");
        for(Player player: squad)
            System.out.println("\t" + player.getName());
    }

    public Club(Owner owner) {
        this.owner = owner;
    }

    public List<Player> getSquad() {
        return squad;
    }

    public void setSquad(List<Player> squad) {
        this.squad = squad;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setLigue(String ligue) {
        this.ligue = ligue;
    }

    public String getLigue() {
        return ligue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
