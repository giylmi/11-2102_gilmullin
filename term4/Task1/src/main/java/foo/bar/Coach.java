package foo.bar;

/**
 * Created by adel on 08.02.14.
 */
public class Coach extends AbstractClubWorker{
    public Coach(String name) {
        super(name);
    }

    @Override
    public void work() {
        System.out.println("Coach " + this.getName() + " trained players all day long!");
    }
}
