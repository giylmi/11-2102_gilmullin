package foo.bar;

/**
 * Created by adel on 08.02.14.
 */
public class Owner extends AbstractClubWorker {
    public Owner(String name) {
        super(name);
    }

    @Override
    public void work() {
        System.out.println("Owner " + this.getName() + " invested money for some top quality transfers!");
    }
}
