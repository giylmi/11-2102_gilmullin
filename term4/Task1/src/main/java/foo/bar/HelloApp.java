package foo.bar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloApp {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        /**
         * club -
         *      в конструктор внедряется ссылка на Owner
         *      через setter внедряются ссылка на Coach,
         *      коллекция squad(List<Player>),
         *      и значения name и ligue
         *
         * player и coach -
         *      внедряются значения name
         */
        Club club = context.getBean(Club.class);
        club.printScheme();

        Player player = (Player) context.getBean("playerAaron");
        player.work();

        Coach coach = context.getBean(Coach.class);
        coach.work();
    }
}
