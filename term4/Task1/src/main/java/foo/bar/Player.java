package foo.bar;

/**
 * Created by adel on 08.02.14.
 */
public class Player extends AbstractClubWorker{
    public Player(String name) {
        super(name);
    }

    @Override
    public void work() {
        System.out.println("Player " + this.getName() + " was brilliant in matchday due to hard trainings!");
    }
}
