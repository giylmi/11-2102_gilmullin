package foo.bar;

/**
 * Created by adel on 08.02.14.
 */
public abstract class AbstractClubWorker {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void work();

    public AbstractClubWorker(String name) {
        this.name = name;
    }
}
