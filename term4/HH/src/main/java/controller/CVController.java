package controller;

import controller.editor.CategoryEditor;
import model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import service.SearchService;
import service.UserService;

@Controller
public class CVController {

    private UserService userService;
    private SearchService searchService;

    @Autowired
    public CVController(UserService userService, SearchService searchService) {
        this.userService = userService;
        this.searchService = searchService;
    }


    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Category.class, new CategoryEditor());
    }

    @RequestMapping("/cv/{id}")
    public ModelAndView getCV(@PathVariable Long id) {
        return new ModelAndView("cv_page", "cv", userService.getCVById(id));
    }

    @RequestMapping("/cv/list")
    public ModelAndView getCV() {
        ModelAndView mv = new ModelAndView("cv_list");
        mv.addObject("cvList", userService.getAllCVs());
        mv.addObject("category", 0);
        mv.addObject("allCategories", searchService.getAllCategories());
        return mv;
    }
}
