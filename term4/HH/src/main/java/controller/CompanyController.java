package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import service.CompanyService;

/**
 * Created by adel on 01.04.14.
 */
@Controller
@RequestMapping(value = "/company")
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @RequestMapping("/{id}")
    public String getCompany(@PathVariable Long id, Model model){
        model.addAttribute("company", companyService.getCompanyById(id));
        return "company_profile";
    }
}
