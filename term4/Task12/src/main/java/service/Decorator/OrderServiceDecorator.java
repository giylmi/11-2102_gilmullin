package service.Decorator;

import service.OrderService;

import java.util.Date;

/**
 * Created by adel on 25.03.14.
 */
public class OrderServiceDecorator implements OrderService {

    private OrderService target;

    public OrderServiceDecorator(OrderService target) {
        this.target = target;
    }

    @Override
    public String getOrder(String orderNumber) {
        System.out.println("LOGGING: OrderService.getOrder(" + orderNumber + ")");
        try{
            return target.getOrder(orderNumber);
        } catch (Exception e){
            System.out.println("LOGGING: OrderService.getOrder(" + orderNumber + ") : thrown Exception. message: " + e.getMessage());
        }
        return null;
    }

    @Override
    public String addOrder(String orderNumber, Long amount) {
        System.out.println("LOGGING: OrderService.addOrder(" + orderNumber + ", " + amount + ")");
        System.out.println("LOGGING: OrderService.addOrder(" + orderNumber + ", " + amount + ") : started : " + new Date().toString());
        String result = target.addOrder(orderNumber, amount);
        System.out.println("LOGGING: OrderService.addOrder(" + orderNumber + ", " + amount + ") : started : " + new Date().toString());
        return result;
    }
}
