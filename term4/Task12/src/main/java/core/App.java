package core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import service.CustomerService;
import service.OrderService;
import service.impl.OrderServiceImpl;

public class App {
	public static void main(String[] args) throws Exception {

		//ApplicationContext appContext =
       //         new ClassPathXmlApplicationContext("Spring-Customer.xml");
        Factory factory = Factory.getInstance();
//		CustomerService customer = (CustomerService) factory.getBean("customerService");
//		customer.addCustomer();
		
	/*	customer.addCustomerReturnValue();
		
		customer.addCustomerAround("customer#1");

        customer.addCustomerThrowException();*/

        OrderService orderService = (OrderService) factory.getOrderServiceDecorator(new OrderServiceImpl());
        orderService.addOrder("order", 100L);
        orderService.getOrder("order");
	}
}