package com.springapp.mvc;

import java.util.ArrayList;

/**
 * Created by adel on 16.02.14.
 */
public class AllSavedRecordsList {

    private static ArrayList<Record> records;

    private AllSavedRecordsList() {

    }

    public static ArrayList<Record> getAllSavedRecordsList() {
        if (records == null)
            records = new ArrayList<Record>();
        return records;
    }

    public static void addRecord(Record record){
        if (records == null)
            records = new ArrayList<Record>();
        records.add(record);
    }
}
