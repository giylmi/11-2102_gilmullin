package com.springapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;

@Controller
public class RecordController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView record() {
        return new ModelAndView("index", "command", new Record());
    }

    @RequestMapping(value = "/addRecord", method = RequestMethod.POST)
    public String addRecord(@ModelAttribute("SpringWeb") Record record, ModelMap model) {
        AllSavedRecordsList.addRecord(record);
        model.addAttribute("records", AllSavedRecordsList.getAllSavedRecordsList());

        return "result";
    }
}