<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Spring MVC Form Handling</title>
</head>
<body>

<h2>Submitted Phone Records Information</h2>
<table border="1px solid black">
    <thead>
        <td></td>
        <td>Name</td>
        <td>Phone Number</td>
    </thead>
    <c:forEach var="record" items="${records}" varStatus="status">

        <tr>
            <td>${status.count}</td>
            <td>${record.name}</td>
            <td>${record.phoneNumber}</td>
        </tr>
    </c:forEach>
</table>
<a href="<c:url value="/"/>">Add Record</a>
</body>
</html>