package com.company;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class ProducerConsumerService {

    public static void main(String[] args) {
        //Creating BlockingQueue of size 10
        BlockingQueue<Message> queue = new ArrayBlockingQueue<>(10);
        Producer producer1 = new Producer(queue, "Adel");
        Producer producer2 = new Producer(queue, "Kate");
        Producer producer3 = new Producer(queue, "Ainur");
        Consumer consumer = new Consumer(queue);
        //starting producer to produce messages in queue
        new Thread(producer1).start();
        new Thread(producer2).start();
        new Thread(producer3).start();
        //starting consumer to consume messages from queue
        new Thread(consumer).start();
        System.out.println("Producers and Consumer has been started");
    }

}