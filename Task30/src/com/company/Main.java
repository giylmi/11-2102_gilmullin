package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Team team = new Team("Russia");
        Team team1 = new Team("USA");
        Team team2 = new Team("China");

        team.start();
        team1.start();
        team2.start();
    }
}

class Team extends Thread{
    private String name;

    Team(String name) {
        this.name = name;
    }

    public void run(){
        for (int i = 1; i <= 4; i++)
            System.out.println("#" + i + ", " + name + ", " + (i * 10));
    }
}
