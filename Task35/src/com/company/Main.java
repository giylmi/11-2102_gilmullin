package com.company;

import java.io.LineNumberReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main implements Callable<Integer>{
    private LineNumberReader reader;
    
    public static void main(String[] args) throws FileNotFoundException, ExecutionException, InterruptedException {
	// write your code here
        ExecutorService service = Executors.newFixedThreadPool(3);
        List<Future<Integer>> list = new ArrayList<Future<Integer>>();

        for (int i = 0; i < 3; i++){
            list.add(service.submit(new Main("out" + i + ".txt")));
        }
        for (Future<Integer> future: list)
            System.out.println(future.get());
        service.shutdown();
    }
    
    public Main(String filename) throws FileNotFoundException {
        this.reader = new LineNumberReader(new FileReader(filename));   
    }
    @Override
    public Integer call() throws Exception {
        int count = 0;
        while (reader.readLine() != null) count++;
        return count;
    }
}
