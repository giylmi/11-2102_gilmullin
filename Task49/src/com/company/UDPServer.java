package com.company;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: adel
 * Date: 12/9/13
 * Time: 1:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class UDPServer {
    private static ArrayList<String> news;

    public static void main(String[] args) throws IOException {
        news = new ArrayList<String>();
        for(int i = 0; i < 10; i++)
            news.add("News " + i);

        try{
            DatagramSocket serverSocket = new DatagramSocket(54321);

            byte[] receiveData;
            byte[] sendData;

            while (true) {
                receiveData = new byte[8];
                DatagramPacket receivePacket =
                        new DatagramPacket(receiveData, receiveData.length);
                System.out.println("Waiting for datagram packet");
                serverSocket.receive(receivePacket);
                String sentence = new String(receivePacket.getData());
                InetAddress IPAddress = receivePacket.getAddress();
                int port = receivePacket.getPort();
                System.out.println("From: " + IPAddress + ":" + port);
                System.out.println("Message: " + sentence);
                if (sentence.equals("Get News")){
                    for (String s: news){
                        sendData = s.toString().getBytes();
                        DatagramPacket sendPacket =
                                new DatagramPacket(sendData, sendData.length, IPAddress, port);
                        serverSocket.send(sendPacket);
                    }
                }
                else {
                    sendData = "Wrong request!\n The only known request is \"Get News\"!".getBytes();
                    DatagramPacket sendPacket =
                        new DatagramPacket(sendData, sendData.length, IPAddress, port);
                    serverSocket.send(sendPacket);
                }
            }
        } catch (SocketException ex) {
            System.out.println("UDP Port 54321 is occupied.");
            System.exit(1);
        }
    }

}
