package com.company;

import org.omg.PortableInterceptor.INACTIVE;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by adel on 10.12.13.
 */
public class ExecutorServiceHappyCounter implements Callable<Double> {
    private BufferedReader br;

    ExecutorServiceHappyCounter(String filename) throws FileNotFoundException {
        this.br = new BufferedReader(new FileReader(filename));
    }

    @Override
    public Double call() throws Exception {
        try {
            String s = br.readLine();
            int num = 0, sum = 0;
            while (s != null){
                num++;
                sum += Integer.valueOf(s.split("-")[1]);
                s = br.readLine();
            }
            return sum * 1.0 / num;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0.0;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException, FileNotFoundException {
        ExecutorService service = Executors.newFixedThreadPool(3);
        List<Future<Double>> list = new ArrayList<Future<Double>>();
        Future<Double> future = service.submit(new ExecutorServiceHappyCounter("in1.txt"));
        list.add(future);
        future = service.submit(new ExecutorServiceHappyCounter("in2.txt"));
        list.add(future);
        future = service.submit(new ExecutorServiceHappyCounter("in3.txt"));
        list.add(future);
        for (Future<Double> future1: list) System.out.println(future1.get());
        service.shutdown();
    }
}
