package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class HappyCounter {
    private double ave[];

    public static void main(String[] args) throws FileNotFoundException {
        // write your code here
        new HappyCounter().run();
    }

    private void run() throws FileNotFoundException {
        ave = new double[3];

        new Thread(new Task("in1.txt", 0)).start();
        new Thread(new Task("in2.txt", 1)).start();
        new Thread(new Task("in3.txt", 2)).start();
//        new Thread(new Task(0, 1002)).start();

        while (Thread.activeCount() != 2) {
        }
        for (double d: ave)
            System.out.println(d);
    }

    class Task implements Runnable {
        private int num;
        private BufferedReader br;

        Task(String filename, int num) throws FileNotFoundException {
            this.num = num;
            this.br = new BufferedReader(new FileReader(filename));
        }

        @Override
        public void run() {
            try {
                String s = br.readLine();
                int num = 0, sum = 0;
                while (s != null){
                    num++;
                    sum += Integer.valueOf(s.split("-")[1]);
                    s = br.readLine();
                }
                ave[this.num] = sum * 1.0 / num;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
